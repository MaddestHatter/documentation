## Notation

## General

*Overview*

Command Line            | Environment              | Type             | Description
----------------------- | ------------------------ | ---------------- | ------------
help                    | `<none>`                 | IMPLICIT_BOOL    | Prints command-line help and close the application.
version                 | `<none>`                 | IMPLICIT_BOOL    | Prints short version information and close the application.
vversion                | `<none>`                 | IMPLICIT_BOOL    | Prints detailed version information and close the application.
osversion               | `<none>`                 | IMPLICIT_BOOL    | Print host operating system information and close the application.

## Development

*Overview*

Command Line            | Environment              | Type             | Description
----------------------- | ------------------------ | ---------------- | ------------
dev-mode                | DEV_MODE                 | IMPLICIT_BOOL    | Acknowledgement flag for running versions in development.

## License

*Overview*

Command Line                | Environment              | Type             | Description
--------------------------- | ------------------------ | ---------------- | ------------
license                     | `<none>`                | IMPLICIT_BOOL    | Prints the license of the application and close the application.
license-third-party         | `<none>`                | IMPLICIT_BOOL    | Prints a list of all third-party licenses included and close the application.
license-third-party-verbose | `<none>`                | IMPLICIT_BOOL    | Prints full information about all third party licenses included and close the application.

## Log

*Overview*

Command Line         | Environment          | Type            | Description
---------------------| ---------------------| --------------- | ------------
log-level            | LOG_LEVEL            | LOG_LEVEL       | Default log level for all specific log levels that are not specified.
log-console-level    | LOG_CONSOLE_LEVEL    | LOG_LEVEL       | Log level for console output.
log-file-level       | LOG_FILE_LEVEL       | LOG_LEVEL       | Log level for file output.
log-file-path        | LOG_FILE_PATH        | FILEPATH        | A file to write log output to.
log-discord-level    | LOG_DISCORD_LEVEL    | LOG_LEVEL       | Log level for discord messages.
log-discord-webhook  | LOG_DISCORD_WEBHOOK  | URL             | A discord webhook URL to send logs on discord.
log-discord-author   | LOG_DISCORD_AUTHOR   | STRING          | The author name of pushed discord messages, useful to distinguish multiple instances pushing logs to the same channel.

## Network

*Overview*

Command Line    | Environment              | Type            | Description
--------------- | ------------------------ | --------------- | ------------
network         | NETWORK                  | FILEPATH        | The blockchain configuration to load. Some networks are integrated and can be referenced.
network-dir     | NETWORK_DIR              | DIRPATH         | Additional lookup directory for network configuration files.

## Breakpad

*Overview*

Command Line    | Environment              | Type            | Description
--------------- | ------------------------ | --------------- | ------------
breakpad-enable | BREAKPAD                 | IMPLICIT_BOOL   | Enables crash dumps to inspect errors causing the application to crash.
breakpad-out    | BREAKPAD_OUT             | DIRPATH         | The directory used to store dumps. New dumps will get a new name and do not override each other.

## SSL

*Overview*

Command Line | Environment              | Type          | Description
------------ | ------------------------ | ------------- | ------------
ssl          | SSL                      | IMPLICIT_BOOL | Enables SSL usage, by default off.
ssl-dir      | SSL_DIR                  | DIRPATH       | Additional lookup directory for ssl files, further referenced as SSL directory.

### Client

*Overview*

Command Line  | Environment              | Type          | Description
------------- | ------------------------ | ------------- | ------------
ssl-trusted   | SSL_TRUSTED              | FILEPATH      | Filepath to a file containing all trusted peers public key, absolute or relative to the SSL directory.
ssl-no-verify | SSL_NO_VERIFY            | IMPLICIT_BOOL | Disables peer verification, considered as insecure.

*Dependencies*

- [Ssl](#ssl)

### Server

Command Line | Environment              | Type            | Description
------------ | ------------------------ | --------------- | ------------
ssl-cert     | SSL_CERT                 | FILEPATH        | Filepath to the certificate file, absolute or relative to the SSL directory. Must be in `PEM` format.
ssl-pk       | SSL_PRIVATE_KEY          | FILEPATH        | Filepath to the private key file, absolute or relative to the SSL directory. Must be in `PEM` format.
ssl-pkp      | SSL_PRIVATE_KEY_PASSWORD | PASSWORD        | Password to decrypt the private key, if less than 8 signs considered as insecure.
ssl-dh       | SSL_DH_PARAM             | FILEPATH        | Filepath to the diffie hellman file, absolute or relative to the SSL directory. Must be in `PEM` format.

*Dependencies*

- [Ssl](#ssl)

## Core

*Overview*

Command Line        | Environment              | Type            | Description
------------------- | ------------------------ | --------------- | ------------
data-dir            | DATA_DIR                 | DIRPATH         | Directory to store all persistent blockchain and network data.
light-node          | LIGHT_NODE               | IMPLICIT_BOOL   | Enables pruning reducing the size of the local blockchain data. Note, light nodes are not full nodes and missing capabilities important for the network.

*Dependencies*

- [Network](#network)
- [Log](#log)

### Checkpoints

Command Line        | Environment              | Type            | Description
------------------- | ------------------------ | --------------- | ------------
checkpoints-disable | CHECKPOINTS              | IMPLICIT_BOOL   | Disables checkpoint usage.
checkpoints-import  | CHECKPOINTS_IMPORT       | FILEPATH        | Imports additional checkpoints from a given file path.

*Dependencies*

- [Core](#core)

### Database

Command Line        | Environment              | Type            | Description
------------------- | ------------------------ | --------------- | ------------
db-threads          | DB_THREADS               | THREAD_COUNT    | Number of threads the database shall use.
db-write-buffer     | DB_WRITE_BUFFER          | BYTES_SIZE      | Maximum size of batches written to the database.
db-read-buffer      | DB_READ_BUFFER           | BYTES_SIZE      | Maximum size of batches read from the database.
db-compression      | DB_COMPRESSION           | COMPRESSION     | Compression method used to lower the memory footprint of data stored on the hard drive.

*Dependencies*

- [Core](#core)

## Peer To Peer

*Overview*

Command Line            | Environment              | Type             | Description
----------------------- | ------------------------ | ---------------- | ------------
p2p-bind-ip             | P2P_BIND_IP              | IPV4             | The IP address your node should be reachable from.
p2p-bind-port           | P2P_BIND_PORT            | PORT             | Specifies the port the node shall listen to.
p2p-hide-port           | P2P_HIDE_PORT            | IMPLICIT_BOOL    | Disables local port announcement to the network.
p2p-local-ip            | P2P_LOCAL_IP             | IMPLICIT_BOOL    | Enables connections from the local network.
p2p-external-port       | P2P_EXTERNAL_PORT        | PORT             | A different port to be used for external access, ie. the port forwarding on the router.
p2p-ban-duration        | P2P_BAN_DURATION         | MINUTES          | Number of seconds a client has time to write all bytes until the connection is considered as timed out.
p2p-ban-auto            | P2P_BAN_AUTO             | IMPLICIT_BOOL    | Number of seconds a client hat time to receive all bytes until the connections are considered as timed out.
p2p-peers-add           | P2P_PEERS_ADD            | HOST ...         | Adds additional peers to the peer list on startup.
p2p-peers-exclusive     | P2P_PEERS_EXCLUSIVE      | HOST ...         | Adds exclusive peers, the node will only connect to those.
p2p-peers-priority      | P2P_PPERS_PRIORITY       | HOST ...         | Adds peers with a higher priority, preferred to connect to.
p2p-peers-seed          | P2P_PEERS_SEED           | HOST ...         | Adds additional seed peers.

*Dependencies*

- [Network](#network)
- [Log](#log)

## Rpc

### Remote

*Overview*

Command Line            | Environment              | Type             | Description
----------------------- | ------------------------ | ---------------- | ------------
rpc-remote-address      | RPC_REMOTE_ADDRESS       | IMPLICIT_BOOL    | The ipv4 address used by the server.
rpc-remote-port         | RPC_REMOTE_PORT          | PORT             | The port configured on the server this client shall connect to.
rpc-remote-host         | `<none>`                 | HOST             | Unified version of address and port to provide both values using a shorthand.
rpc-remote-access-token | RPC_REMOTE_ACCESS_TOKEN  | TOKEN            | An access token sent to the server on every request to authorized the client.

*Dependencies*

- [Network](#network)
- [Log](#log)
- [Ssl Client](#client)
    - [Ssl](#ssl)

### Server

*Overview*

Command Line             | Environment              | Type             | Description
------------------------ | ------------------------ | ---------------- | ------------
rpc-server               | RPC_SERVER               | IMPLICIT_BOOL    | Enables a remote command server. Which services are enabled on that instance depends on the corresponding service configurations.
rpc-server-cors          | RPC_SERVER_CORS          | WILDCARD         | Enables cross-server HTTP access, from all servers matching the provided wildcard.
rpc-server-bind-ip       | RPC_SERVER_BIND_IP       | IPV4             | Specifies which ip this server is listening to. Using 0.0.0.0 is considered insecure.
rpc-server-bind-port     | RPC_SERVER_BIND_PORT     | PORT             | Port this server listens to.
rpc-server-access-token  | RPC_SERVER_ACCESS_TOKEN  | TOKEN            | An additional token required from each client to be authenticated.
rpc-server-read-timeout  | RPC_SERVER_READ_TIMEOUT  | SECONDS          | Number of seconds a client has time to write all bytes until the connection is considered as timed out.
rpc-server-write-timeout | RPC_SERVER_WRITE_TIMEOUT | SECONDS          | Number of seconds a client hat time to receive all bytes until the connections are considered as timed out.
rpc-server-read-limit    | RPC_SERVER_READ_LIMIT    | BYTES_SIZE       | The maximum number of bytes per second read by the server from a client connection.
rpc-server-write-limit   | RPC_SERVER_WRITE_LIMIT   | BYTES_SIZE       | The maximum number of bytes per second written by the server to the client.

*Dependencies*

-  [Network](#network)
-  [Log](#log)
-  [Ssl Server](#server)
    -  [Ssl](#ssl)

## Services

### Public Node

*Overview*

Command Line               | Environment              | Type             | Description
-------------------------- | ------------------------ | ---------------- | ------------
public-node-disable        | PUBLIC_NODE              | IMPLICIT_BOOL    | Disables the public node service. Note the server only starts if the RPC server is enabled.
public-node-mining-disable | PUBLIC_NODE_MINING       | IMPLICIT_BOOL    | Disables the public node mining endpoint. Miners are not able to obtain or push blocks to this server.
public-node-fee-address    | PUBLIC_NODE_FEE_ADDRESS  | WALLET_ADDRESS   | Public address users need to pay the fee too to use the public node.
public-node-fee-view-key   | PUBLIC_NODE_FEE_VIEW_KEY | SECRET_KEY       | The view secret key of the wallet address. This enables the server to validate users paid the fee of the server.
public-node-fee-fee        | PUBLIC_NODE_FEE_AMOUNT   | ATOMIC_AMOUNT    | Number of atomic units charged for every transaction on this server.

*Dependencies*

-  [Network](#network)
-  [Log](#log)
-  [Rpc Server](#server_1)
    -  [Ssl Server](#server)
        -  [Ssl](#ssl)
-  [Core](#core)
    -  [Checkpoints](#checkpoints)
    -  [Database](#database)

### Block Explorer

*Overview*

Command Line             | Environment              | Type             | Description
------------------------ | ------------------------ | ---------------- | ------------
block-explorer-enable    | BLOCK_EXPLORER           | IMPLICIT_BOOL    | Enables the block explorer service. Note the service is only available if the RPC server is enabled.

*Dependencies*

-  [Network](#network)
-  [Log](#log)
-  [Rpc Server](#server_1)
    -  [Ssl Server](#server)
        -  [Ssl](#ssl)
-  [Core](#core)
    -  [Checkpoints](#checkpoints)
    -  [Database](#database)

## Applications

### Wallet

*Overview*

Command Line    | Environment     | Type           | Description
--------------  | --------------- | -------------- | ------------
w , wallet      | WALLET_FILE     | FILEPATH       | The wallet file to be used.
p , password    | WALLET_PASSWORD | PASSWORD       | The wallet password to be used to open a given wallet file without a password prompt.
g , generate    | WALLET_GENERATE | IMPLICIT_BOOL  | Generates a new wallet if not present on startup.
s , seed        | WALLET_SEED     | STRING         | A seed for wallet generation. For the same seed, the generated wallet is always the same.
r , remote      | WALLET_REMOTE   | IMPLICIT_BOOL  | Disables the integrated node and tries to connect to a remote node instead.

*Integrates*

- [General](#general)
- [License](#license)
- [Breakpad](#breakpad)
- [Log](#log)
- [Core](#core)
    - [Network](#network)
    - [Database](#database)
- [Peer To Peer](#peer_to_peer)
- [Rpc Remote](#remote)
    - [Ssl Client](#client)
        - [Ssl](#ssl)

### Miner

*Overview*

Command Line         | Environment           | Type           | Description
-------------------- | --------------------- | -------------- | ------------
a , address          | MINER_ADDRESS         | WALLET_ADDRESS | The address to mine to, block rewards will be sent to the given address.
t , threads          | MINER_THREADS         | NUMBER         | Number of threads to use, 0 will launch the maximum number of threads that can run concurrently on the current hardware.
u , update-interval  | MINER_UPDATE_INTERVAL | MILLISECONDS   | Sets the interval of update polling, higher values decrease you hash rate on the main chain, lower values increase the load on the remote node.
r , report-show      | MINER_REPORT_SHOW     | IMPLICIT_BOOL  | Enables automatic status reporting.
i , report-interval  | MINER_REPORT_INTERVAL | SECONDS        | Sets the number of seconds between consecutive status reports.
l , block-limit      | MINER_BLOCK_LIMIT     | NUMBER         | Automatically closes the application once the number of blocks has been mined by this instance.
p , panic            | MINER_PANIC           | IMPLICIT_BOOL  | Forces the application to crash if it stucked.
n , none-interactive | NONE_INTERACTIVE      | IMPLICIT_BOOL  | Disables all interactive commands.

*Integrates*

- [General](#general)
- [License](#license)
- [Breakpad](#breakpad)
- [Log](#log)
- [Network](#network)
- [Rpc Remote](#remote)
    - [Ssl Client](#client)
        - [Ssl](#ssl)


### Daemon

*Overview*

Command Line                 | Environment      | Type               | Description
---------------------------- | -----------------| ------------------ | ------------
genesis-generate-transaction | `<none>`         | IMPLICIT_BOOL      | Generates the genesis transaction for the configuration file, prints it and closes the application.
genesis-reward-targets       | `<none>`         | WALLET_ADDRESS ... | Receiver of a premine used to generate the genesis transaction.
n , none-interactive         | NONE_INTERACTIVE | IMPLICIT_BOOL      | Disables all interactive commands.

*Integrates*

- [General](#general)
- [License](#license)
- [Breakpad](#breakpad)
- [Log](#log)
- [Core](#core)
    - [Network](#network)
    - [Database](#database)
- [Peer To Peer](#peer_to_peer)
- [Rpc Server](#server_1)
    - [Ssl Server](#server)
        - [Ssl](#ssl)
- [Public Node](#public_node)
- [Block Explorer](#block_explorer)


### Sync

*Overview*

Command Line           | Environment     | Type           | Description
---------------------- | --------------- | -------------- | ------------
i , import             | `<none>`        | IMPLICIT_BOOL  | Indicates import usage, the application will set up a node database importing the file.
e , export             | `<none>`        | IMPLICIT_BOOL  | Indicates export usage, the application will read a database or connect to a remote to generate a dump of the current blockchain state.
f , file               | SYNC_FILE       | FILEPATH       | The dump file to read or write depending on import or export usage.
r , remote             | SYNC_REMOTE     | IMPLICIT_BOOL  | Enables a remote export, the application will connect to the remote and download the blockchain state.
b , batch-size         | SYNC_BATCH_SIZE | NUMBER         | Number of blocks written to one batch in the file.
d , density            | SYNC_DENSITY    | NUMBER         | Density of included checkpoints. Higher values increase import time.
t , trunc              | SYNC_TRUNC      | IMPLICIT_BOOL  | Allows the application to overwrite an existing dump in case of export.

*Integrates*

- [General](#general)
- [License](#license)
- [Breakpad](#breakpad)
- [Log](#log)
- [Core](#core)
    - [Network](#network)
    - [Database](#database)
- [Rpc Remote](#remote)
    - [Ssl Client](#client)
        - [Ssl](#ssl)

### Benchmark

*Overview*

Command Line           | Environment     | Type           | Description
---------------------- | --------------- | -------------- | ------------
s , silent             | `<none>`        | IMPLICIT_BOOL  | Disables progress messages.
t , threads            | `<none>`        | NUMBER         | Number of threads to benchmark, 0 will test multiple thread variations up to the maximum concurrent thread count supported by the hardware.
b , blocks             | `<none>`        | NUMBER         | Number of blocks to hash for every thread. Lower values will yield less reliable hash rates and higher values will yield in longer execution time.
f , format             | `<none>`        | STRING         | The format to be used to print the benchmark summary.
a , algorithm          | `<none>`        | STRING         | The algorithm to benchmark.
