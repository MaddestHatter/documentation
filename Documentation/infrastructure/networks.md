# Networks

Xi runs multiple blockchains for testing and evaluation purposes. Each blockchain is a different network and serves a different purpose. Note none of these networks are compatible with each other and you are not able to make any transactions across networks or mine on multiple chains at once. For most users, the, so-called, `MainNet` is the only network relevant as this is the blockchain most users are connected to and represents the production state of Xi.

!!! warning "Be Aware Of Different Networks"
    If you are awaiting a transaction for an exchange make sure you are on the main network. You are always able to verify what network your daemon is connected to with the `status` command. This should print `on MainNet synced`. Coins are not interchangeable between newtworks. If you are not sure, you can always validate your transaction at our [block explorer](https://explorer.xiproject.io).

In the following, we give a small introduction to the purpose of every network type.

??? tip "Explicitly State Your Network"
    All network dependent executables have a command line flag for specifying the network you want to connect to. If you want to make sure you are always connected to the right network, regardless of the [distribution channel](./channels.md), make sure to specify the network flag `--network <network name>` whenever you start one of the network-dependent applications. In an example for the main network, you need to provide `--network MainNet` (case sensitive).

!!! info "Persistent Storage Is Network Specific"
    Please note you can run multiple daemons on different networks using the same data directory. Every persistent storage file is stored with a network specific name and will not override any data stored for other network types.

## MainNet

The main network `--network MainNet` is the production network of the Xi blockchain. If you are not trying to test any newly developed features you should always connect to this network. All users using the stable release will connect to this network by default.

## StageNet

The staging network `--network StageNet` is a testing network for release candidates. These versions are most likely to be the next stable release but stay in an evaluation phase to find potential bugs before releasing it as a production version for the main network. This network will not reset and shall stay compatible with newer versions.

Please note the staging network may crash or get partial rollbacks. Further, you can test new upcoming feature, or existing features, safely here without putting your coins on the main network in danger.

## TestNet

The testing network `--network TestNet` is an unstable network that is used to experiment and test new features that may not be compatible with the main network or only partially implemented. If you are not testing new features in development you may not want to connect to this network in any way.

Developers are free to reset the network, breaking compatibility to running test network daemons or shut it down at any time.

## LocalTestNet

The local testing network `--network LocalTestNet` is a network that is not reachable by anyone other than the developer currently running it. This network is a private version of the test network to simplify testing routines for developers.
