# Channels

Galaxia uses continuous integration and delivery to ensure higher product quality while reducing the update cycle time window at the same time. For end users that means less bugs and a faster update cycle. To ensure this Galaxia has multiple layers of releases according to how well tested including changes are. In the following we will introduce the different channels together with the implications of choosing one of them.

??? tip "Identify The Channel You Are Using."
    If you are not certain what kind of version you are currently using you can always run your local version using the `--vversion` (verbose version) flag. The application should list a table of release informations including the distribution channel. For an example `xi-daemon --vversion` or `xi-wallet --vversion`. Please note you can also use the `version` command in a running daemon instance to obtain these informations.

## Overview

Let us start with an overview of all channels. Every channel has an identifier, referenced as the channels name/type an according git branch and a default network type. For more information on network types please refer to our documentation on [networks](../networks).

!!! attention "Be Aware Of Default Network Types"
    If you want to try a new versions feature be aware that you will not connect to the main network by default. This is on purpose to prevent damage that may harm users running by accident into a none stable release.

| Channel | Git Branch        | Default Network                    | Summary                                                                          |
|:--------|:------------------|:-----------------------------------|:---------------------------------------------------------------------------------|
| stable  | master            | [MainNet](../networks/#mainnet)    | The production version of Galaxia.                                                    |
| beta    | release-candidate | [StageNet](../networks/#stagenet)  | Pending staging version with upcoming stable features for Galaxia.                    |
| edge    | develop           | [TestNet](../networks/#testnet)    | Testing version with finished features that are not fully tested yet.            |
| clutter | all others        | [TestNet](../networks/#testnet)    | Contains features in development that may are not stable or harm the blockchain. |


## Life Cycles

In order to understand release channels more accurate it is important to understand how new feature and fixes propagate the release cycle. In the following we describe the evolution of fixes and new features and how they propagate the release channels of Galaxia.

### Features

Once a new feature starts to be developed it gets a new designated branch. As `master`, `release-candidate` and `develop` are protected and reserved the according release will automatically be in the `clutter` channel. Once the development of the feature finished the changes get merged into the `develop` branch and thus will be contained in the `edge` channel release. This version will then deployed as part of the [test network](../networks/#testnet). Changes will remain on this release until developers and reviewers are satisfied by the quality and all potential bugs or inconvenient behavior stand fixed. Next the feature will be pulled into the `release-candidate` branch, this will be contained by the `beta` channel release. `beta` releases gets deployed to the [staging network](../networks/#stagenet). This implicitly means that the upcoming feature is compatible to the main network and is now in its last evaluation phase before being deployed as production code. The feature remains in this release until all developers/reviewers are satisfied by the quality of the new feature. After the new feature stayed in the `beta` release for at least three to fourteen days (depending on the impact of the change), without any further modification, the feature is ready to be deployed to production. Finally, the feature gets merged into the `master` channel and will be part of the `stable` channel and thus part of the production code of Galaxia used by the [main network](../networks/#mainnet).

### Fixes

If a bug is contained by production code it may be vital to release a new version as fast as possible. Therefore fixes have a different channel propagation. Every fix is always applied to the current stable version on a new branch. That means the fix is not part of the production code but ensures compatibility to it. After the problem was resolved and developers/reviewers are satisfied with the fix changes skip the `edge` channel and go directly into the `beta` channel for evaluation. Fixes have a lower restriction on the time window they must remain stable in the [staging network](../networks/#stagenet). Based on the impact of the change and the importance of the bug those changes can be merged into production code after staying two to forty-eight hours stable.
