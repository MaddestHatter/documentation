# Running Galaxia Using Docker

## Install

First, we need to install Docker and docker-compose. Note install scripts are taken from [docker.com](https://docs.docker.com/install/). If you run in any problems please check out their instructions or text us on [discord](https://discord.gg/CcY8Nqh). Please click your corresponding host operating system to see install instructions.

??? "CentOS"
    ```bash
    sudo yum install -y               \
        yum-utils                     \
        device-mapper-persistent-data \
        lvm2
    sudo yum-config-manager --add-repo                           \
        https://download.docker.com/linux/centos/docker-ce.repo
    sudo yum install docker-ce docker-compose
    sudo usermod -aG docker $USER
    ```

??? "Debian"
    ```bash
    sudo apt-get update
    sudo apt-get install             \
        apt-transport-https         \
        ca-certificates             \
        curl                        \
        gnupg2                      \
        software-properties-common
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
    sudo add-apt-repository                                       \
    "deb [arch=amd64] https://download.docker.com/linux/debian \
    $(lsb_release -cs)                                         \
    stable"
    sudo apt-get update
    sudo apt-get install docker-ce docker-compose
    sudo usermod -aG docker $USER
    ```

??? "Fedora"
    ```bash
    sudo dnf -y install dnf-plugins-core
    sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
    sudo dnf install docker-ce docker-compose
    sudo systemctl start docker
    sudo usermod -aG docker $USER
    ```

??? "Ubuntu"
    ```bash
    sudo apt update
    sudo apt-get install -y        \
        apt-transport-https        \
        ca-certificates            \
        curl                       \
        software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic $(lsb_release -cs) stable"
    sudo apt-get update
    sudo apt-get install -y docker-ce docker-compose
    sudo usermod -aG docker $USER
    ```

??? "macOS"
    1. [Download](https://store.docker.com/editions/community/docker-ce-desktop-mac) the installer.
    2. Double-click Docker.dmg to open the installer, then drag Moby the whale to the Applications folder.

        ![](https://docs.docker.com/docker-for-mac/images/docker-app-drag.png)

    3. Double-click Docker.app in the Applications folder to start Docker. (In the example below, the Applications folder is in “grid” view mode.)

        ![](https://docs.docker.com/docker-for-mac/images/docker-app-in-apps.png)

??? "Windows"
    1. [Download](https://store.docker.com/editions/community/docker-ce-desktop-windows) the installer.
    2. Double-click Docker for Windows Installer.exe to run the installer.
    3. Follow the install wizard to accept the license, authorize the installer, and proceed with the install.

        You are asked to authorize Docker.app with your system password during the install process. Privileged access is needed to install networking components, links to the Docker apps, and manage the Hyper-V VMs.

    4. Click Finish on the setup complete dialog to launch Docker.

    5. Start Docker for Windows

        ![](https://docs.docker.com/docker-for-windows/images/docker-app-search.png)

## Setup

Once docker is installed we can work with it using a terminal. On Linux and MacOS you can search for the application `terminal` and run it. On Windows right click the windows icon in your taskbar and select `Windows PowerShell`.

Now, let us verify docker is up and running by executing the following commands in the terminal.

```bash
docker -v
docker-compose -v
docker run --rm -it hello-world
```

??? info "Show expected output"
    ```bash
    > docker -v
    Docker version 18.06.1-ce, build e68fc7a
    > docker-compose -v
    docker-compose version 1.22.0, build f46880fe
    > docker run --rm -it hello-world
    Unable to find image 'hello-world:latest' locally
    latest: Pulling from library/hello-world
    d1725b59e92d: Pull complete
    Digest: sha256:0add3ace90ecb4adbf7777e9aacf18357296e799f81cabc9fde470971e499788
    Status: Downloaded newer image for hello-world:latest

    Hello from Docker!
    This message shows that your installation appears to be working correctly.

    To generate this message, Docker took the following steps:
    1. The Docker client contacted the Docker daemon.
    2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
        (amd64)
    3. The Docker daemon created a new container from that image which runs the
        executable that produces the output you are currently reading.
    4. The Docker daemon streamed that output to the Docker client, which sent it
        to your terminal.

    To try something more ambitious, you can run an Ubuntu container with:
    $ docker run -it ubuntu bash

    Share images, automate workflows, and more with a free Docker ID:
    https://hub.docker.com/

    For more examples and ideas, visit:
    https://docs.docker.com/get-started/

    ```

??? tip "Known problems on Linux"
    Note on Linux you may get a permission error similar to 
    ```bash
    docker: Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Post http://%2Fvar%2Frun%2Fdocker.sock/v1.26/containers/create: dial unix /var/run/docker.sock: connect: permission denied.
    See 'docker run --help'.
    ```
    . If so you may did not add yourself to the docker group (`sudo usermod -a -G docker $USER`) or need to log out and back in.

Once docker is running we can start to setup Galaxia. Therefore we need a configuration file. You can find a working example in our [github-repository](https://github.com/registry.gitlab.com/galaxia-project/blockchain/xi/blob/dev/docker-compose.yml). Download the [file](https://github.com/registry.gitlab.com/galaxia-project/blockchain/xi/blob/dev/docker-compose.yml) and store it somewhere where you want your xi data being stored. Here are some example scripts for faster setup.

```bash tab="Linux"
mkdir -p ~/.xi
cd ~/.xi
wget https://gitlab.com/galaxia-project/blockchain/xi/snippets/1792142/raw
```

```bash tab="macOS"
mkdir -p ~/.xi
cd ~/.xi
curl -O https://gitlab.com/galaxia-project/blockchain/xi/snippets/1792142/raw
```

```PowerShell tab="Windows"
New-Item -ItemType Directory -Path $HOME\.xi | Set-Location
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
curl -Uri https://gitlab.com/galaxia-project/blockchain/xi/snippets/1792142/raw -OutFile .\docker-compose.yml
```

Finally, we need to replace the default wallet address with our own. Therefore open the `docker-compose.yml` with a text editor. You may do this using your already opened terminal.

* _Linux_: `open -e ./docker-compose.yml`
* _MacOS_: `open ./docker-compose.yml`
* _Windows_: `Start-Process .\docker-compose.yml`

Exchange the two occurrences of the current wallet address `xi...` with your own.

??? info "Show expected file content"
    ```yaml
    version: '3.3'

    services:
    daemon:
        image: registry.gitlab.com/galaxia-project/blockchain/xi
        container_name: xi_daemon
        restart: always
        expose:
        - "31018"
        - "21018"
        volumes:
        - ./.data:/data
        entrypoint: xi-daemon --data-dir /data --rpc-bind-ip 0.0.0.0 --fee-address Xizwr3KCvHXdSGi8kQtbdQfUKPZbqaYtZAHdh4BGqp63CkGZBGdaZWiQJysqqXJTnSPckRccAzNMaf2U3Lt2ZU9W4axVJqoatg --fee-amount 2

    miner:
        image: registry.gitlab.com/galaxia-project/blockchain/xi
        container_name: xi_miner
        restart: always
        depends_on: 
        - daemon
        entrypoint: xi-miner --address Xizwr3KCvHXdSGi8kQtbdQfUKPZbqaYtZAHdh4BGqp63CkGZBGdaZWiQJysqqXJTnSPckRccAzNMaf2U3Lt2ZU9W4axVJqoatg --daemon-host daemon --log-level 3
    ```

??? tip "I have no wallet address yet, let me make one."
    Fortunately we have docker and ca do this quite simple. We just start a new instance of a wallet.
    * _Linux_: `docker run --rm -it -v "$(pwd):/xi" -w "/xi" registry.gitlab.com/galaxia-project/blockchain/xi xi-wallet`
    * _Linux_: `docker run --rm -it -v "$(pwd):/xi" -w "/xi" registry.gitlab.com/galaxia-project/blockchain/xi xi-wallet`
    * _Windows_: `docker run --rm -it -v "$(Convert-Path $PWD):/xi" -w "/xi" registry.gitlab.com/galaxia-project/blockchain/xi xi-wallet`

    ??? info "Show expected output"
        ```bash
        + ----------------------------------------------------------------------------------------------------- +
        |                                                                         *                             |
        |    ________       .__                .__         __________                   __               __     |
        |   /  _____/_____  |  | _____  ___  __|__|____    \______   \_______  ____    |__| ____   _____/  |_   |
        |  /   \  ___\__  \ |  | \__  \ \  \/  /  \__  \    |     ___/\_  __ \/  _ \   |  |/ __ \_/ ___\   __\  |
        |  \    \_\  \/ __ \|  |__/ __ \_>    <|  |/ __ \_  |    |     |  | \(  <_> )  |  \  ___/\  \___|  |    |
        |   \______  (____  /____(____  /__/\_ \__(____  /  |____|     |__|   \____/\__|  |\___  >\___  >__|    |
        |          \/     \/          \/      \/       \/                          \______|    \/     \/        |
        |                                                                                                       |
        |      *                       ____  ___.___                                                            |
        |                              \   \/  /|   |                                                           |
        |                       ______  \     / |   |                                           *               |
        |                      /_____/  /     \ |   |                                                           |
        |                              /___/\  \|___|         *                                                 |
        |               *                    \_/                                                                |
        |   *                                                                                                   |
        + ----------------------------------------------------------------------------------------------------- +

        XI v0.0.0.0 (1cdd45f)
        This software is distributed under the General Public License v3.0

        Copyright 2019, The Galaxia Project Developers

        Additional Copyright(s) may apply, please see the included LICENSE file for more information.
        If you did not receive a copy of the LICENSE, please visit:
        https://gitlab.com/galaxia-project/blockchain/xi/blob/develop/LICENSE

        Waiting for remote connection...

        1      open                     Open a wallet already on your system
        2      create                   Create a new wallet
        3      seed_restore             Restore a wallet using a seed phrase of words
        4      key_restore              Restore a wallet using a view and spend key
        5      view_wallet              Import a view only wallet
        6      exit                     Exit the program
        ```

    Now type `create` and follow the instructions. You can name your wallet however you like but for now, we assume it is called `xi`.

    ??? info "Show example wallet creation"
        ```bash
        + ----------------------------------------------------------------------------------------------------- +
        |                                                                         *                             |
        |    ________       .__                .__         __________                   __               __     |
        |   /  _____/_____  |  | _____  ___  __|__|____    \______   \_______  ____    |__| ____   _____/  |_   |
        |  /   \  ___\__  \ |  | \__  \ \  \/  /  \__  \    |     ___/\_  __ \/  _ \   |  |/ __ \_/ ___\   __\  |
        |  \    \_\  \/ __ \|  |__/ __ \_>    <|  |/ __ \_  |    |     |  | \(  <_> )  |  \  ___/\  \___|  |    |
        |   \______  (____  /____(____  /__/\_ \__(____  /  |____|     |__|   \____/\__|  |\___  >\___  >__|    |
        |          \/     \/          \/      \/       \/                          \______|    \/     \/        |
        |                                                                                                       |
        |      *                       ____  ___.___                                                            |
        |                              \   \/  /|   |                                                           |
        |                       ______  \     / |   |                                           *               |
        |                      /_____/  /     \ |   |                                                           |
        |                              /___/\  \|___|         *                                                 |
        |               *                    \_/                                                                |
        |   *                                                                                                   |
        + ----------------------------------------------------------------------------------------------------- +

        XI v0.0.0.0 (1cdd45f)
        This software is distributed under the General Public License v3.0

        Copyright 2019, The Galaxia Project Developers

        Additional Copyright(s) may apply, please see the included LICENSE file for more information.
        If you did not receive a copy of the LICENSE, please visit:
        https://gitlab.com/galaxia-project/blockchain/xi/blob/develop/LICENSE

        1      open                     Open a wallet already on your system
        2      create                   Create a new wallet
        3      seed_restore             Restore a wallet using a seed phrase of words
        4      key_restore              Restore a wallet using a view and spend key
        5      view_wallet              Import a view only wallet
        6      exit                     Exit the program

        What would you like to do?: create
        What would you like to call your new wallet?: xi
        Give your new wallet a password: ***
        Confirm your new password: ***
        Welcome to your new wallet, here is your payment address:
        Xizwr3KCvHXdSGi8kQtbdQfUKPZbqaYtZAHdh4BGqp63CkGZBGdaZWiQJysqqXJTnSPckRccAzNMaf2U3Lt2ZU9W4axVJqoatg

        Please copy your secret keys and mnemonic seed and store them in a secure location:
        Private spend key:
        f6ccd11dd580891b0777281a920539f43f385aed2d812a85ba27adf2b3ac1001

        Private view key:
        755dbb1f7efe6039b1b174a310529001c92c0bbf3ffee113bbad7d13ebaa2e0e

        Mnemonic seed:
        pizza ulcers absorb mews exult hire foyer fleet icon lamb eden deity tusks amused winter enough feline spiders aphid abort voyage comb uttered vane voyage

        If you lose these your wallet cannot be recreated!

        It looks like xi-daemon isn't open!

        Ensure xi-daemon is open and has finished initializing.
        If it's still not working, try restarting xi-daemon. The daemon sometimes gets stuck.
        Alternatively, perhaps xi-daemon can't communicate with any peers.

        The wallet can't function fully until it can communicate with the network.

        1      try_again                Try to connect to the node again
        2      continue                 Continue to the wallet interface regardless
        3      exit                     Exit the program

        What would you like to do?: exit
        Shutting down...
        Shutting down node connection...
        Bye.
        ```

    Make sure to copy or print the outputted `Private spend key`, `Private view key` and `Mnemonic seed`. If you ever lose your created wallet file and do not have access to those keys your account, and all `XIG` in it will be lost. Thus, seriously: backup, please. Additionally, the wallet creation will output a `payment address`. This is the public address you will need to use to configure `docker-compose.yml`.

Now everything is set up and we can start the miner.
```bash
docker-compose up -d
```
??? info "Show expected output"
    ```bash
    > docker-compose up -d
    Creating xi_daemon ... done
    Creating xi_miner  ... done
    ```

Congratulations. You started to mine XIG and collaborating with our community to create a truly private cryptocurrency we can trust. Thank you.

## Monitoring

You may notice not very much changed. So how can you see what is actually happening in the background? First, let us list our created machines.
```bash
docker ps
```

??? info "Show expected output"
    ```bash
    > docker ps
    CONTAINER ID        IMAGE                                               COMMAND                     CREATED             STATUS              PORTS                  NAMES
    7e1df0d95719        registry.gitlab.com/galaxia-project/blockchain/xi   "xi-miner --address Xij…"   4 minutes ago       Up 11 seconds                              xi_miner
    2366a12c1412        registry.gitlab.com/galaxia-project/blockchain/xi   "xi-daemon --data-d…"       4 minutes ago       Up 4 minutes        21018/tcp, 31018/tcp   xi_daemon
    ```

We can see two machines are currently running, one named `xi_miner` and the other `xi_daemon`. The `daemon` is responsible for interactions with other users in the blockchain while the `miner` requests new tasks from the `daemon` and pushes newly found blocks. Let us have a look at what they are doing, starting with the daemon.

```bash
docker logs xi_daemon
```
??? info "Show expected output"
    ```bash
    > docker logs xi_daemon
    2018-Nov-15 16:52:37.778167 INFO

    + ----------------------------------------------------------------------------------------------------- +
    |                                                                         *                             |
    |    ________       .__                .__         __________                   __               __     |
    |   /  _____/_____  |  | _____  ___  __|__|____    \______   \_______  ____    |__| ____   _____/  |_   |
    |  /   \  ___\__  \ |  | \__  \ \  \/  /  \__  \    |     ___/\_  __ \/  _ \   |  |/ __ \_/ ___\   __\  |
    |  \    \_\  \/ __ \|  |__/ __ \_>    <|  |/ __ \_  |    |     |  | \(  <_> )  |  \  ___/\  \___|  |    |
    |   \______  (____  /____(____  /__/\_ \__(____  /  |____|     |__|   \____/\__|  |\___  >\___  >__|    |
    |          \/     \/          \/      \/       \/                          \______|    \/     \/        |
    |                                                                                                       |
    |      *                       ____  ___.___                                                            |
    |                              \   \/  /|   |                                                           |
    |                       ______  \     / |   |                                           *               |
    |                      /_____/  /     \ |   |                                                           |
    |                              /___/\  \|___|         *                                                 |
    |               *                    \_/                                                                |
    |   *                                                                                                   |
    + ----------------------------------------------------------------------------------------------------- +

    XI v0.0.0.0 (1cdd45f)
    This software is distributed under the General Public License v3.0

    Copyright 2018, The Galaxia Project Developers

    Additional Copyright(s) may apply, please see the included LICENSE file for more information.
    If you did not receive a copy of the LICENSE, please visit:
    https://gitlab.com/galaxia-project/blockchain/xi/blob/develop/LICENSE


    2018-Nov-15 16:52:37.778561 INFO    Program Working Directory: xi-daemon
    2018-Nov-15 16:52:37.779238 INFO    Loading Checkpoints for faster initial sync...
    2018-Nov-15 16:52:37.779328 INFO    Loaded 0 default checkpoints
    2018-Nov-15 16:52:37.779414 INFO    Opening DB in /data/DB
    2018-Nov-15 16:52:37.780438 INFO    DB not found in /data/DB. Creating new DB...
    2018-Nov-15 16:52:37.783293 INFO    Initializing core...
    2018-Nov-15 16:52:37.783738 INFO    Core initialized OK
    2018-Nov-15 16:52:37.783793 INFO    Initializing p2p server...
    2018-Nov-15 16:52:37.783969 INFO    Generated new peer ID: 6203143618660274375
    2018-Nov-15 16:52:37.784015 INFO    Binding on 0.0.0.0:21018
    2018-Nov-15 16:52:37.784071 INFO    Net service binded on 0.0.0.0:21018
    2018-Nov-15 16:52:37.784109 INFO    Attempting to add IGD port mapping.
    2018-Nov-15 16:52:41.788852 INFO    No IGD was found.
    2018-Nov-15 16:52:41.788999 INFO    P2p server initialized OK
    2018-Nov-15 16:52:41.789167 INFO    Starting core rpc server on address 0.0.0.0:31018
    2018-Nov-15 16:52:41.789313 INFO    Core rpc server started ok
    2018-Nov-15 16:52:41.789386 INFO    Starting p2p net loop...
    2018-Nov-15 16:52:41.789443 INFO    Starting node_server
    2018-Nov-15 16:53:02.473143 INFO    [42.51.41.62:21018 OUT] Your xiCoin node is syncing with the network (0.01% complete) You are 7504 blocks (10 days) behind the current peer you're connected to. It's not a race, grab a coffee.
    2018-Nov-15 16:53:02.473315 INFO    New Top Block Detected: 7505
    2018-Nov-15 16:53:04.228516 INFO    Block 100 (14aaaf90e4701c3833fe06e75d93106e5228ab5dd4b0a39b5d19f8b04caa06ec) added to main chain
    2018-Nov-15 16:53:05.609868 INFO    Block 200 (6661694af013638bab138c7bcfe2f69e754be9d34cd5340c02f498abd6748ced) added to main chain
    2018-Nov-15 16:53:06.315927 INFO    Block 300 (dda27428921eb5236cfeba135211e57f3c338ceba5ee904c6c05f2c4681b241a) added to main chain
    2018-Nov-15 16:53:06.853880 INFO    Block 400 (a51da7c6c168808f8a9c88b31608aff5daa5b15f605a6a35cf2c98d65f05f8e5) added to main chain
    2018-Nov-15 16:53:07.761290 INFO    Block 500 (ac256e1244ced06d0d2a5897e5d746bb86c7314eb204865a217cb857ba4279f1) added to main chain
    ```

The daemon started synchronizing with the blockchain. Once it has finished, it will be part of the xi community!

??? info "I started my own daemon. Is this secure?"
    Yes, this is absolutely secure. Docker creates 'sandboxes', so even if the miner or daemon would have been hacked they cannot escape their cage. Furthermore; docker-compose creates an isolated network and only the miner has access to it.

??? info "What happens if I shut down my computer or restart it, or the daemon/miner crashes?"
    We configured the machines as a service. Whenever you start/restart your computer they will start as well. Furthermore, the blockchain is locally cached on your computer and you will not need to download it every time. In addition, docker will track the status of your services and if they crash for any reason they will be restarted immediately.

Unfortunately, while our daemon is syncing the blockchain, the miner cannot receive any job. We can inspect his progress using:
```bash
docker logs xi_miner
```
??? info "Show expected output"
    ```bash
    > docker logs xi_miner
    2018-Nov-15 16:52:38.424644 INFO    [MinerManager] requesting mining parameters
    2018-Nov-15 16:52:38.425979 WARNING [MinerManager] Couldn't get block template: TcpConnector::connect, connection failed
    2018-Nov-15 16:52:38.426016 WARNING [MinerManager] Couldn't connect to daemon: TcpConnector::connect, connection failed
    2018-Nov-15 16:53:08.426095 INFO    [MinerManager] requesting mining parameters
    2018-Nov-15 16:53:08.427146 WARNING [MinerManager] Couldn't get block template: Coreisbusy
    Fatal: Coreisbusy
    ```

We will now have to wait sometime for the blockchain to sync. You can monitor its progress by executing `docker logs xi_daemon`. Once it's finished, the miner will start with all cores available, and we can monitor our hash rate using `docker logs xi_miner`.

??? info "Show example log of a running miner"
    ```bash
    > docker logs xi_miner
    ...
    2018-Nov-15 17:39:34.573516 INFO    [Miner] Starting mining for difficulty 254850806
    2018-Nov-15 17:39:34.573574 INFO    [Miner] Thread 0 started at nonce: 1034367710
    2018-Nov-15 17:39:34.573601 INFO    [Miner] Thread 1 started at nonce: 1034367711
    2018-Nov-15 17:40:32.113810 INFO    [MinerManager] Mining at 62 H/s
    2018-Nov-15 17:40:34.635662 INFO    [Miner] Starting mining for difficulty 244391903
    2018-Nov-15 17:40:34.635724 INFO    [Miner] Thread 0 started at nonce: 3023423309
    2018-Nov-15 17:40:34.635755 INFO    [Miner] Thread 1 started at nonce: 3023423310
    2018-Nov-15 17:41:04.687688 INFO    [Miner] Starting mining for difficulty 248816920
    2018-Nov-15 17:41:04.687764 INFO    [Miner] Thread 0 started at nonce: 3674148510
    2018-Nov-15 17:41:04.687795 INFO    [Miner] Thread 1 started at nonce: 3674148511
    ```

## Managing

For all commands managing our services `docker-compose` will search for a `docker-compose.yml`. There we assume you already opened a terminal and changed your active directory, as mentioned in the setup section.

* _downloading updates_:

    Our _continuous integration_ system automatically tests new releases and updates the containers. Therefore it is meaningful to update the services from time to time and getting the latest improvements.

    ```bash
    docker-compose down
    docker-compose pull
    docker-compose up -d
    ```

    Note if you configured a fixed version number for your services you will need to pick your desired version from [gitlab](https://gitlab.com/galaxia-project/blockchain/xi/container_registry) and update `docker-compose.yml` accordingly.

* _stop mining_: `docker-compose stop`
* _start mining_: `docker-compose up -d`
* _delete the service_: `docker-compose down`
