# Static Rewards

Many other blockchains decide, on startup, to pay out an initial number of coins to initiators in order to ensure the maintenance and invest in further development of the blockchain. You may have heard about this in the context of a `premine` and `genesis block`. This means the entire community agrees on an initial state of the blockchain containing the payout of those coins to a built-in address. However, this approach comes with some risks for the community we at Galaxia solved with the implementation of static rewards. In the following, we introduce some downsides of the, previously mentioned, premine approach and explain how static reward works as well as how they solve the described issues.

## Premine Issues

### Immutable Genesis Block

Genesis block describes the first block of a blockchain. As blockchains always validate alterations based on the previous state of the blockchain. Thus they need to have one initial block all agree on encapsulating the initial state as they are not able to validate it against a previous one. As a result, the genesis block can never be changed and must always be valid as the entire chain builds upon it. This leads to the problem that you will never be able to remove the initial coins paid out because the transaction is contained in the genesis block. Further, if the community ever decides to split/fork to a divergent implementation based on the current state of the project the initiator will hold his coins on both chains even if he does not contribute to the fork by any means. In that case, the new community will not be able to use the premine for any further development of the fork which conflicts with the intent of the premine in the first place.

### Discrepancy Of Concerns

The initiator/s of the blockchain hold all coins and take responsibility for managing investments for the good of the blockchain. However, this may not align with the major interests of the community. Unfortunately, there is no way for the community to vote for it or take a partial amount of the premine to invest in their interest. The community depends on the goodwill of the initiator/s and have a no way to enforce actions even if they build the majority, except for leaving the project entirely and as previously mentioned a fork will benefit the initiator and will not give any access for the premine to the new community.

### Vulnerabilities Of View Keys

Some of the previously mentioned issues are a more or less privacy coin specific as you are not able to validate or view at any time how the premine is managed. One common solution is publishing so-called view keys. These keys allow users to see every transaction received by the wallet containing the premine but does not show outgoing transactions. Users are only able to validate what has been sent to that wallet. Note that this approach does not prevent the community from any issue mentioned, it is only to validate statements are correct it does not prevent the holder to do whatever he thinks is best with their coins.

Additionally, this approach harms the security of the network. Most blockchains of the CryptoNote family uses a feature called `mixins`. In short, this technology uses coins from other users and mix them with your own such that everyone can validate that the holder of the coin spent his own but cannot know which one it was. However, if a user mixes their coins with coins from a wallet that has published its view keys you can validate that these coins just served for obfuscation and reduce the number of possible coins used for the transaction.

### Lower Mining Reward

The introduced CryptoNote emission method lowers the amount over newly generated coins, for every block, over time based on the already generated coins and the target of the maximum number of coins to be generated over the entire lifecycle of the blockchain. As a premine generates an initial supply thus the amount of generated coins for each block is lowered from the beginning, which disadvantage miners adopting early to secure the blockchain.

## Introducing Static Rewards

Static rewards introduce a new method of coin generation. Besides the normal coin generation method, referred to as `mining reward`, there is a second generating coins to a built-in address, the static reward address. This static reward is optional and can be enabled/disabled or retargeted to a new address at any time as long as the network agrees on that and finds consensus. You can think of static rewards like a premine that pays out over time and can be changed by a fork if the community is not satisfied by the envelope of the blockchain led by a holder of the static reward address and decides to split.

### Mutable State

The configuration of a static reward includes three properties.

1. The time window static rewards are generated. If the blockchain state is not within this time window the blockchain is not able to generate coins using static rewards.

2. The number of coins generated for each block.

3. The address being eligible to use the generated coins.

Please note that this configuration is built in and cannot be changed dynamically. Thus the entire community can view the configuration and decide themselves if they accept it or not without the possibility of someone changing this configuration on a running system at any time. If this configuration gets changed the community must agree with the new configuration and follow the fork introduced.

### Possibility To Fork

With a mutable configuration, the community can always fork and disable or retarget static rewards. The previous static reward address will not get any reward further. Note that this is not true for already generated coins, these will also be available on the fork. It is the responsibility of the initiator to choose the configuration such that the amount of generated coins align with the effort put into the blockchain at that point of time. Further once the community forks they can redirect the static reward to a new address to be able to maintain development and invest further into the blockchain.

### No View Keys Required

As all agree on the static reward and coins are generated over time the community knows that the managers of the static reward address do only have a limited amount of coins. It is up to the manager to ensure he pays out the generated coins frequently and state the usage clearly and transparent to the community. Finally, the holder is not committed to publishing view keys in order to retain a value of trust from his community and the network will not be harmed by none anonymous coins mixed.

### Higher Mining Reward

Without a premine, the initial total supply is lower yielding a higher reward for early adopters, as mentioned [here](#lower-mining-reward). This way miners will not be disadvantaged which may attract miners to adopt faster making the blockchain more robust and secure.

## Conclusion

Static rewards give us a great tool to split responsibilities and give more power to the community for the development of the blockchain. We at Galaxia believe that this is the best way and hope we can convince the community with the usage of the static reward. We are excited about discussions in the community finding the best distribution of this resource satisfying the majority.
