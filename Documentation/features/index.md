# Features Overview

## Native SSL

SSL (Secure Sockets Layer) is a technology to ensure data you send stays a secret between you and your peer. Even if you start multiple services on the same computer and only connect them locally third-party applications are able to read your messages if you do not use SSL. For that reason Galaxia embedded native support for SSL to enable every user to secure their data and wallets with much less effort than required without it. You can read more about the SSL feature [here](./ssl).

## P2P Banning

P2P (peer-to-peer) is an architecture used to organize the workload of the blockchain, or the distribution of new information, to all peers connected to the blockchain. In case of an attack, one or multiple peers may try to harm the network. Galaxia has built-in sanity checks to detect those attacks and automatically ban them from connecting to your local instance. This makes the network more stable and secure. To read more about P2P Banning have a look [here](./p2pbanning).

## Static Rewards

To ensure further development and support of a blockchain it is common to pay out the initial founders with an initial amount of coins. This is often referred to as a `premine`. It is up to the founders how they invest these coins for the betterment of the blockchain. This introduces some potential risk to the community. For example the founder may not invest the coins in the interest of the community to enhance the technology and tries to pay out himself once the cryptocurrency is tradable on an exchange. As a result, the community must have a high level of trust in the founder depending on him acting fairly with the responsibilities given. To help aide in this Galaxia introduces the concept of static rewards. This will eliminate these risks giving more power to the community ensuring continuous progess of the blockchaind development. For more informations about static rewards read [here](./staticreward).

## Crash Reports

Sometimes things do not work as expected for a user. From a developer point of view tracking down failures in the applications can be a very time-consuming task as they have to evaluate the context of the user and try to reproduce it in order to fix the problem. Galaxia introduces the integration of [breakpad](https://github.com/google/breakpad/blob/master/docs/getting_started_with_breakpad.md) which serves as a development tool shipped to every user. Using breakpad Galaxia developers are able to identify problems occurring for users faster enabling them to provide better support. If you want to know more about how this works please continue [here](./crashreports.md).
