# P2P Banning

The blockchain consists of many users interconnected with their daemons, further referenced as peers. It is a decentralized architecture where everyone helps everyone to validate every change made like transactions, coins generated and so forth. They all propagate pieces of information about the current knowledge of the blockchain and find consensus. This way the blockchain stays stable and all peers agree on the current state of the network without having to trust a single data provider.

From time to time it may appear that peers are not playing by the rules the rest of the peers agree on. This can happen for multiple reasons, in an example, someone created a new blockchain and did not change the network configuration correctly or an attacker created a customized version that is trying to harm the network by sending malicious data.

These peers, not playing by the rules, may slow down the entire network as everyone needs to validate their false data. To overcome this problem and protect Galaxia from denial of service attacks we implemented a penalty system for peers autonomously protecting the network.

## The Penalty System

The penalty system applies a penalty score to a peer whenever it breaks the rules. The number of points varies depending on how crucial the violation was. The following table gives an overview of penalty scores applied for a specific type of violation.

| Type                 | Penalty | Description                                                                                                              |
|:----------------- ---|:--------|:-------------------------------------------------------------------------------------------------------------------------|
| Wrong Network        | 10      | The peer connected from a different network.                                                                             |
| Income Violation     | 5       | The peer is pushing data while he should not.                                                                            |
| Deprecated Version   | 2       | The peers has an incompatible version and needs to update.                                                               |
| Handshake Failure    | 2       | The peer was not able to establish the connection correctly.                                                             |
| Invalid Response     | 4       | The peer promised to serve some specific data but did not once asked.                                                    |
| Invalid Request      | 4       | The peer sent an invalid request asking for information you never promised to have or contains none understandable data. |
| Block Failure        | 5       | The peer tried to push an invalid blockchain state.                                                                      |
| Transaction Failure  | 5       | The peer sent an invalid transaction.                                                                                    |
| No Response          | 2       | The peer was expected to respond to a request but did not.                                                               |
| Exceptional          | 4       | The request of a peer lead to an exception.                                                                              |

Please note that the network also slowly lowers the penalty score of peers if they continuously act as expected by other peers.

## The Ban System

Once a peer reaches a penalty score of twenty or above the connection to that peer is shut down and he will not be able to connect with your local node within the next twenty-four hours (default configuration). Note that all bans and penalties are local decisions of every daemon and are not shared within the network. This is vital as there should not be anyone with the power to ban peers for others. Further, even if the seed nodes ban peers you still will be able to connect to those and their connection info will propagate the network regardless of their penalty or ban status. This way we ensure that everyone can decide themselves to ban a peer or not or disabling the ban system at all.
