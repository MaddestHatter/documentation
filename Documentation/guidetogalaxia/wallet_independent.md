#The cli wallet  - xi-wallet - Independed Mode

The xi-wallet program has everything inside to be used independent. This means, on startup it will start a integrated daemon , 
connect it to the galaxia network,syncs the wallet with the network and takes care while doing transfers 
or simply creating a new wallet and other possible tasks.

## Prerequisite
Download the [latest release files](https://releases.galaxia-project.com/stable/) fitting your operating system, 
extract the archive file and navigate to the folder with the extracted files.



Usage (every info below presuppose to be done from folder "\bin"):  
 
```File-Explorer tab="GUI"
simply double click the "xi-wallet.exe" or "xi-wallet" depending on your OS; 
```

```PowerShell tab="Windows"
open a command line "cmd" or "powershell" 
and type .\xi-wallet.exe in it and hit ENTER
```

```bash tab="Linux"
.\xi-wallet
```






