#The cli wallet  - xi-wallet - 'Advanced' Linked to Remote Mode

As described in [Wallet Independend](wallet_independent.md) , 
the wallet is able to run without any other software. 

A deeper look into xi-wallet programm shows up, that its a container consisting of 2 parts.

- the wallet client itself and         (client part)
- a integrated (light) daemon aka node (core part) 

This in memory, explains that it is possible to use "the wallet client part" only and 
connect it to a seperate running node, in example . 

## Prerequisite : Running and synced daemon
Setup a and start up a node on your local machine, like discribed [here](node.md)

## Connect the wallet to your local daemon 

./xi-wallet -r
