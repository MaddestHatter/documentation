# Software overview

The Galaxia Blockchain consits of serveral seperate programms. 

To get the software components you need to download 
[latest release files](https://releases.galaxia-project.com/stable/) 
fitting your operating system, and extract the archive .

after extracting you will find serveral executables in the "bin" folder : 

- xi-daemon
- xi-miner
- xi-pgservice
- xi-sync
- xi-wallet


## xi-daemon - 
use case : 
connect to galaxia blockchain , sync blockchain to local space,
connect xi-miner , xi-wallet or xi-pgservice to the galaxia blockchain.

## xi-wallet - the cli wallet

use case : 
create a galaxia wallet address ,  
manage and do transfers , i.e. send and receive transactions 
address book 

## xi-miner -  the mining application
use case :
miner application for galaxia blockchain,  
creates block hashes, keeps blockchain move forward. 
 

## xi-pgservice - cli payment gate service app
use case :
manages one (or more) service wallets and creates a interface (rpc, json) for using 
this wallet with other application, i.e. as woocomerce backend. 


## xi-sync 
use case : 



